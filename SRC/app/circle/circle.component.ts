import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {Circle} from './circle';
import {CircleService} from './../services/circle.service';

@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css']
})
export class CircleComponent implements OnInit {


  circles: object[];
  @Output() selectedCircle = new EventEmitter<any>();
  constructor(private circleservice: CircleService) { }


  ngOnInit() {
  /*  let circle=new Circle();
    this.circles=circle.getCircles();*/
    this.getCircles();
  }

  /*getCircles(){
    this.circles=this.circleservice.getCircles();
  }*/

  getCircles(){
    console.log('In component');
    this.circleservice.getCircles().subscribe( circle =>{
      this.circles=circle.json();
    });

  }

  selectCircle(circleData:any){
    console.log('circle name:'+circleData.circleName);
    const currentCircleValue={type:'circle',value:circleData};
    this.selectedCircle.emit(currentCircleValue);
  }
}
