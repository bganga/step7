import { Component, OnInit,Input } from '@angular/core';
import { Message } from './message';
import { MessageService } from './../services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {


  messages: object[];
  message:any;
  userdata:string;
  circle:string;
  receiver:string;
  type:string;

  @Input() messageobj:any;

  constructor(private messageservice:MessageService) { }

  ngOnInit() {
    /*let message=new Message();
    this.messages=message.getMessages();*/
    //  this.getMessages();

  }

  ngOnChanges(value){
    this.messageservice.getMessagesByCircle(value.messageobj.currentValue.circleName).subscribe(data=>{
      this.messages=data.json();
    });

  }

/*  getMessages(){
    this.messages=this.messageservice.getMessages();
  }
*/


}
