export class Message{

     senderName: string;
  	 receiverId: string;
  	 circleName: string;
  	 postedDate:  any;
  	 message: string;

     messages : object[];

     constructor(){
       this.messages=[{senderName: 'Suresh',receiverId: 'Sarvan',circleName: 'Java',postedDate: new Date(),message: 'Hi'},
                      {senderName: 'Suresh',receiverId: 'Sarvan',circleName: 'Spring boot',postedDate: new Date(),message: 'Hello'}
                     ];
     }

     getMessages(){

       return this.messages;
     }
}
