
export class User{

  username: string;
  name: string;
  users: object[];

  constructor(){
    this.users=[
      {username:'Suresh',name:'Surr'},
      {username:'Sarvan',name:'sarv'},
      {username:'Ram',name:'Ram'},
      {username:'Arun',name:'arun'}
    ];
  }

  getUser(){
    return this.users;
  }
}
