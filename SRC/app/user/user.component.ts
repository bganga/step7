import { Component, OnInit,Output,EventEmitter  } from '@angular/core';
import { User } from './user';
import { UserService } from './../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {

   users: object[];
   @Output() selectedUser = new EventEmitter<any>();
  constructor(private userservice:UserService){

  }

  ngOnInit() {
    //let user=new User();
    //this.users=user.getUser();
    this.getUsers();
  }

  /*getUsers(){
    this.users=this.userservice.getUsers();
    console.log("size:"+this.users.length);
  }*/

  getUsers(){
    console.log('In user component');
    this.userservice.getUsers().subscribe( data =>{
      this.users=data.json();
    });

  }

  selectUser(userData:any){
    console.log('user name:'+userData.username);
    const currentUserValue={type:'user',value:userData};
    this.selectedUser.emit(currentUserValue);
  }
}
