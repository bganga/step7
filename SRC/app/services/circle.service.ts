import { Injectable } from '@angular/core';
import { Circle } from './../circle/circle';
import { Http } from '@angular/http';

@Injectable()
export class CircleService {

  constructor(private http : Http) { }

  private CIRCLE_SERVICE_BASE_URL ='http://localhost:8085/api/circle';

/*  getCircles(): any{
     let circle=new Circle();
     return circle.getCircles();
  }*/

  getCircles(){
    console.log('In Circle service');
    return this.http.get(this.CIRCLE_SERVICE_BASE_URL);
  }

}
