import { Injectable } from '@angular/core';
import{ User } from './../user/user';
import{ Http } from '@angular/http';

@Injectable()
export class UserService {


  private USER_SERVICE_BASE_URL='http://localhost:8082/api/user';
  private CREATEUSER='http://localhost:8082/user';
  private GET_USERS='http://localhost:8082/api/users';
  private AUTHENTICATE='http://localhost:8082/login';

  public headerObj=new Headers({
    'Authorization':'Bearer '+localStorage.getItem('token')
  });
  constructor(private http:Http) { }

  /*getUsers() : any{
    let obj=new User();
    return obj.getUser();
  }*/

  getUsers(){
    console.log('In User Service get all users');
    return this.http.get(this.GET_USERS,{headers:this.headerObj;});
  }

  registerUser(form){
    return this.http.post(this.CREATEUSER,form.value);
  }

  authenticateUser(form){
    return this.http.post(this.AUTHENTICATE,form.value);
  }

}
