import { Injectable } from '@angular/core';
import { Message } from './../message/message';
import { Http } from '@angular/http';

@Injectable()
export class MessageService {

  constructor(private http : Http) { }

  private MESSAGE_SERVICE_BASE_URL ='http://localhost:8082/api/message';

  circleName:string;

  getMessages():any{
    let message=new Message();
    return message.getMessages();
  }

  getMessagesByCircle(circleName:string){
    this.circleName=circleName;
    console.log("In message service:"+circleName);
    return this.http.get(this.MESSAGE_SERVICE_BASE_URL+'/getMessagesByCircle/'+circleName+'/'+2);
  }

  sendMessageToCircle(message:any){
    return this.http.post(this.MESSAGE_SERVICE_BASE_URL+'/sendMessageToCircle/'+this.circleName,message);
  }

}
