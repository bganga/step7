
export class Circle{
  circleName: string;
 	creatorId: string;
  circles: object[];

  constructor(){
    this.circles=[{circleName:'SpringBoot',creatorId: 'surr'},
                  {circleName : 'MERN',creatorId: 'sarvan'}
    ];
  }

  getCircles(){
    return this.circles;
  }
}
