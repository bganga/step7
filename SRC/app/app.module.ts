import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import { FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';

import {UserService} from './services/user.service';
import {CircleService} from './services/circle.service';
import { MessageService } from './services/message.service';

import { AppComponent } from './app.component';
import { CircleComponent } from './circle/circle.component';
import { UserComponent } from './user/user.component';
import { MessageComponent } from './message/message.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    CircleComponent,
    UserComponent,
    MessageComponent,
    LoginComponent,
    RegistrationComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    RouterModule.forRoot([
      {
        path:'',component:LoginComponent
      },
      {
        path:'register',component:RegistrationComponent
      },
      {
        path:'dashboard',component:(localStorage.getItem('token')===null)?LoginComponent:DashboardComponent
      }
    ])
  ],
  providers: [UserService,CircleService,MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
