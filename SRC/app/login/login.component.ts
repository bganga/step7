import { Component, OnInit ,Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;

  constructor(private userservice:UserService,private router:Router) { }

  ngOnInit() {
  }

  onClick(){
    console.log("Button clicked");
  }

  onSubmit(form:NgForm){
    console.log(form.value);
    this.userservice.authenticateUser(form).subscribe(data=>{
      if(data.status===200){
        this.router.navigate(['/dashboard']);
        location.reload();
        localStorage.setItem('token',data.json().token);
        localStorage.setItem('username',form.value.username);
      }

      }
    );
  }

}
