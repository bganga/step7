import { Component,Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular demo by Surr';

  currentSelectedCircle:any;

  onSelect(data){
    console.log(data.value);
    this.currentSelectedCircle=data.value;
    console.log('Circle in App Component:'+this.currentSelectedCircle.circleName);
  }

  



}
