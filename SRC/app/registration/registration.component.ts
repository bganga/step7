import { Component, OnInit } from '@angular/core';
import{ NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './../services/user.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  username:string;
  password:string;
  email:string;
  name:string;

  constructor(private userservice : UserService,private router : Router) { }

  ngOnInit() {
  }

  onClick(){
    console.log("Button clicked");
  }

  onSubmit(form: NgForm){
    console.log(form.value);

    this.userservice.registerUser(form).subscribe(data=>{
      if(data.status===201){
        this.router.navigate(['']);
      }
      console.log("Registeration success"+data.status);

    });
  }
}
